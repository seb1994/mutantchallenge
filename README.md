# MUTANT-SERVICE-API
Este proyecto sirve para que validar si una secuencia de ADN corresponde a un mutante o no, adicionalmente permite consultar estadísticas de las validaciones realizadas

## Guia del proyecto
La información de pruebas y contenido general del proyecto se encuentra en la siguiente ruta de este repositorio:
| Descripción | Ruta |
| ------ | ------ |
| Codigo | /Project Code |
| Documento de diseño | /Project Documents |
| Pruebas de Cobertura | /Project Documents/Coverage Test |
| Pruebas de Carga | /Project Documents/ Jmeter Test |
| Imgs Proyecto corriendo en local | /Project Documents/ Deploy |

## Proyecto en local
Este proyecto fue desarrollado en Java 11 con Spring Boot en el IDE STS (Spring Tool Suite), mas sin embargo su base es maven. Por lo cual si requieres ejecutarlo en local debes:
* Clonar el proyecto
* Ya descargado, configurarlo en un IDE y realizar la respectiva ejecución.
* En caso de que no desees en IDE y si cuentas con Maven instalado, basta con posicionarte en el directorio raiz, abrir un cmd y jecutar el siguiente comando: 
  ```mvnw.cmd spring-boot:run```
  En caso de querer iniciarlo sin los test cases:
  ```mvnw.cmd spring-boot:run -Dmaven.test.skip=true```
  
* Debes repetir este paso para cada uno de los tres proyectos y ejecutarlos en el siguiente orden (1. eureka, 2. mutant-service 3. apigateway)

* El puerto por defecto para eureka es 8787 y para apigateway es 8788, el puerto de mutant service lo toma de manera aleatoria

* Los puertos los puedes cambiar en el archivo de ***application.properties*** agregando o modificando la siguiente propiedad server.port=Nuevo Puerto
  
* Verificar que todo este funcionando, revisar cada uno de los terminales e ingresar al navegador a http://localhost:8787/ para revisar eureka 
   * Si tienes dudas puedes revisar la ruta 
      |/Project Documents/Deploy 
  
* Para probar debes ir al navegador a http://localhost:8788/api/mutants/stats o importar el proyecto de prueba a postman
   * El proyecto de postman (MutantMELI.postman_collection.json) lo puedes encontrar en 
     | /Project Documents/Deploy 
  
## Proyecto en la nube
Este proyecto se ha desplegado en nube AWS y lo puedes usar a través de los siguientes endpoints o importando el proyecto de postman (MutantMELI.postman_collection.json):
| Descripción | EndPoint |
| ------ | ------ |
| Obtener las estadisticas | http://3.129.243.53:8788/api/mutants/stats |                             
| Verificar ADN | http://3.129.243.53:8788/api/mutants/stats |

