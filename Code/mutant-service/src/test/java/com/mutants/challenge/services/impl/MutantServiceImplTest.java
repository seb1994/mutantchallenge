package com.mutants.challenge.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mutants.challenge.model.dto.MutantRequestDto;
import com.mutants.challenge.services.IMutantService;
import com.mutants.challenge.utils.Constants;

@SpringBootTest
class MutantServiceImplTest {
	
	@Autowired
	private IMutantService mutantService;
	
	@Test
	void shouldReturnErrorEmptyMessageWhenDnaSequenceProvidesIsNull() {
		String[] dnaSequence = null;
		MutantRequestDto mutantRequestDto = new MutantRequestDto();
		mutantRequestDto.setDna(dnaSequence);
		assertEquals(Constants.EMPTY_MESSAGE, mutantService.isValidDnaInput(mutantRequestDto.getDna()));
	}
	
	@Test
	void shouldReturnFalseWhenDnaSequenceProvidesIsEmpty() {
		String[] dnaSequence = {};
		MutantRequestDto mutantRequestDto = new MutantRequestDto();
		mutantRequestDto.setDna(dnaSequence);
		assertFalse(mutantService.isMutant(mutantRequestDto));
	}

	@Test
	void shouldCreateMutantRequestDtoFromEmptyConstructorAndPopulateAtributesOneByOne() {
		String[] dnaSequence = {"AGAGAG","TCTCTC","AGAGAG","TCTCTC","AGAGAG","TCTCTC"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto();
		mutantRequestDto.setDna(dnaSequence);
		assertFalse(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnFalseWhenDnaSequenceProvidedHasMenorLenght() {
		String[] dnaSequence = {"AGA","TCT","AGA"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto();
		mutantRequestDto.setDna(dnaSequence);
		assertFalse(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnFalseWhenNoCoincidences() {
		String[] dnaSequence = {"AGAGAG","TCTCTC","AGAGAG","TCTCTC","AGAGAG","TCTCTC"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertFalse(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnTrueWhenHasTwoCoincidences() {
		String[] dnaSequence = {"ATGCAA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnTrueWhenHasTwoOrMoreCoincidencesInHorizontal() {
		String[] dnaSequence = {"GTAAAA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnTrueWhenHasTwoOrMoreCoincidencesInVertical() {
		String[] dnaSequence = {"ACGCGA","CTGTGC","TTATGT","ATAAGG","CTCCTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnTrueWhenHasTwoOrMoreCoincidencesInLeftOblique() {
		String[] dnaSequence = {"ATGCGA","CAGTGC","TTATGT","ATAAGG","CCTCTA","TCATTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnTrueWhenHasTwoOrMoreCoincidencesInRightOblique() {
		String[] dnaSequence = {"CTGCGA","CAGTAC","TTAAGT","AGAAGG","CCCCTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnErrorNonValidMatrixWhenIsNotSymmetricMatrix() {
		String[] dnaSequence = {"ATGCGA","CAGT","TTATGT","AAGG","CCCCTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertEquals(Constants.NON_VALID_MATRIX, mutantService.isValidDnaInput(mutantRequestDto.getDna()));		
	}
	
	@Test
	void shouldReturnErrorMessageHasNumbersWhenMatrixHasNumbers() {
		String[] dnaSequence = {"ATGCGA","CAG8GC","TTATGT","AGAAGG","CC1CTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertEquals(Constants.MATRIX_HAS_NUMBERS, mutantService.isValidDnaInput(mutantRequestDto.getDna()));
	}
	
	@Test
	void shouldReturnFalseWhenMatrixHasSpecialCharacters() {
		String[] dnaSequence = {"ATGC#A","CAGTGC","TTATGT","A.AAGG","CCCCTA","T$ACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertEquals(Constants.MATRIX_HAS_NUMBERS, mutantService.isValidDnaInput(mutantRequestDto.getDna()));
	}
	
	@Test
	void shouldReturnFalseWhenMatrixHasLowercaseCharacters() {
		String[] dnaSequence = {"ATgcGA","CAGTGC","TTATGT","AGAAgg","CcCCTA","tCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertFalse(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnFalseWhenMatrixHasInvalidCharacters() {
		String[] dnaSequence = {"ATGCGA","CAGTGC","TTATPT","AGAAGG","JCCCTA","TCACTG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertFalse(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnTrueWhenSearchSequenceInGreaterMatrix() {		
		String[] dnaSequence = {"ATGCGAA","CAGTGCA","TTATGTG","AGAAGGC","CCCCTAA","TCACTGG","AGAAGTC"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnErrorNonValidCharWhenSearchSequenceInGreaterMatrixWithNoValidCharacters() {		
		String[] dnaSequence = {"ATGCGAAC","CAGTGCAG","TTATGTGT","AGAAGGCA","CCCCTAAG","TCACTGGC","AGAAGTCG","AGAAGYCG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertEquals(Constants.NON_VALID_CHAR_MATRIX, mutantService.isValidDnaInput(mutantRequestDto.getDna()));
	}
	
	@Test
	void shouldReturnTrueWhenSearchSequenceInGreaterMatrixWithValidCharacters() {		
		String[] dnaSequence = {"ATGCGAAC","CAGTGCAG","TTATGTGT","AGAAGGCA","CCCCTAAG","TCACTGGC","AGAAGTCG","AGAAGCCG"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertTrue(mutantService.isMutant(mutantRequestDto));
	}
	
	@Test
	void shouldReturnErrorNonValidMinInputWhenMatrixIsNotValid() {		
		String[] dnaSequence = {"CAG","TTA","AGA"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertEquals(Constants.NON_VALID_MINIM_INPUT, mutantService.isValidDnaInput(mutantRequestDto.getDna()));
	}
	
	@Test
	void shouldReturnErrorInputHasLowerCharWhenMatrixWithNoUpperCharacters() {		
		String[] dnaSequence = {"cagt","ttat","agaa","cccc"};
		MutantRequestDto mutantRequestDto = new MutantRequestDto(dnaSequence);
		assertEquals(Constants.MATRIX_HAS_LOWER_CHAR, mutantService.isValidDnaInput(mutantRequestDto.getDna()));
	}
	
	
}
