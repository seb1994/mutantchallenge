package com.mutants.challenge.services.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mutants.challenge.model.dto.MutantStatisticDto;
import com.mutants.challenge.model.entity.MutantStatistic;
import com.mutants.challenge.repository.MutantRepository;
import com.mutants.challenge.services.IMutantStatisticService;

@SpringBootTest
class MutantStatisticServiceImplTest {
	
	@Autowired
	private IMutantStatisticService mutantStaticService;
	
	@Autowired
	private MutantRepository mutantRepository;
	
	@Test
	void shouldReturnTrueStatQueryNoRecords() {
		mutantRepository.deleteAll();
		List<MutantStatistic> lstStats =  (List<MutantStatistic>)mutantRepository.findAll();
		System.out.println("Cantidad datos: " +lstStats.size());
		assertTrue(lstStats.isEmpty());		
	}
	
	@Test
	void shouldReturnTrueWhenRecordSaved() {
		MutantStatistic mutantStatistic = new MutantStatistic();
		mutantRepository.save(mutantStatistic);
		List<MutantStatistic> lstStats = (List<MutantStatistic>)mutantRepository.findAll();
		assertTrue(lstStats.size()>0);
		
	}	
	
	@Test
	void shouldReturnNotNullStatObject() {
		MutantStatisticDto mutantStatisticDto = mutantStaticService.findMutantStatistcis();
		assertNotNull(mutantStatisticDto);
	}
	
	@Test
	void shouldReturnNotNullStatObjectWhenEmptyTable() {
		mutantRepository.deleteAll();
		MutantStatisticDto mutantStatisticDto = mutantStaticService.findMutantStatistcis();
		assertNotNull(mutantStatisticDto);
	}
	
	@Test
	void shouldReturnTrueWhenRationGreaterThanZeroWhenManyValuesHumanAndMutant() {
		mutantRepository.deleteAll();
		String[] dnaFakeSequence = {"ATGCAA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
				
		for (int i = 1; i<=10; i++) {
			MutantStatistic mutantStatistic = new MutantStatistic();
			mutantStatistic.setMutantCheckingResult(new Random().nextBoolean());
			mutantStatistic.setRequestDate(new Date());
			mutantStatistic.setDnaRequest(Arrays.toString(dnaFakeSequence));
			mutantRepository.save(mutantStatistic);
		}
		
		MutantStatisticDto MutantStatisticDto = mutantStaticService.findMutantStatistcis();
		assertTrue( MutantStatisticDto.getRatio() > 0 );
	}
	
	@Test
	void shouldReturnWhenRatioValueIsInfinite() {
		mutantRepository.deleteAll();
		String[] dnaFakeSequence = {"ATGCAA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
				
		MutantStatistic mutantStatistic = new MutantStatistic();
		mutantStatistic.setMutantCheckingResult(true);
		mutantStatistic.setRequestDate(new Date());
		mutantStatistic.setDnaRequest(Arrays.toString(dnaFakeSequence));
		mutantRepository.save(mutantStatistic);
	
		MutantStatisticDto MutantStatisticDto = mutantStaticService.findMutantStatistcis();
		assertTrue( MutantStatisticDto.getRatio() == 0 );
	}
	
	@Test
	void shouldReturnNotNullWhenCreateFaceMutantStatisticDto() {
		
		MutantStatisticDto mutantStatisticDto = new MutantStatisticDto();
		mutantStatisticDto.setCountHumanDna(0);
		mutantStatisticDto.setCountMutantDna(0);
		mutantStatisticDto.setRatio(0);
		
		assertNotNull(mutantStatisticDto.getRatio()==0);
	}

}
