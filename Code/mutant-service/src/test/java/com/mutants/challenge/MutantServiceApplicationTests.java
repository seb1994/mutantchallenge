package com.mutants.challenge;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MutantServiceApplicationTests {

	@Test
	void contextLoads() {
		MutantServiceApplication.main(new String[] {});
	}

}
