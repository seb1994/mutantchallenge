package com.mutants.challenge.utils;

import java.util.ArrayList;
import java.util.List;

public class UtilsMutant {

	public static String[] transposeDna(String[] dna) {

		String[] transposed = new String[dna.length];

		for (int i = 0; i < dna.length; i++) {
			StringBuilder dnaBase = new StringBuilder();
			dnaBase.setLength(0);
			for (int j = 0; j < dna.length; j++) {
				dnaBase.append(dna[j].charAt(i));
			}
			transposed[i] = dnaBase.toString();
		}

		return transposed;
	}

	public static String[] oblDna(String[] dna) {

		List<String> listDna = new ArrayList<String>();

		for (int i = 0; i <= dna.length - Constants.DNA_MIN_NUMBER_PATTERN; i++) {		
			StringBuilder dnaBaseUp = new StringBuilder();
			StringBuilder dnaBaseDown = new StringBuilder();
			dnaBaseUp.setLength(0);
			dnaBaseDown.setLength(0);
			for(int j = 0; j < dna.length - i; j++) {
				if(i == 0) {
					dnaBaseUp.append(dna[j].charAt(j));
				}else {
					dnaBaseUp.append(dna[j].charAt(i + j));
					dnaBaseDown.append(dna[i + j].charAt(j));
				}
							
			}
			
			listDna.add(dnaBaseUp.toString());
			
			if(dnaBaseDown != null && dnaBaseDown.length() > 0) {
				listDna.add(dnaBaseDown.toString());				
			}
			
		}

		return listDna.toArray(new String[0]);
	}

	public static String[] reverseDna(String [] dna) {
		String[] reverse = new String[dna.length];
		int k = 0;
		
		for(int i = dna.length - 1; i >= 0; i--) {
			reverse[k] = dna[i];
			k++;			
		}
		
		return reverse;
	}
	
}
