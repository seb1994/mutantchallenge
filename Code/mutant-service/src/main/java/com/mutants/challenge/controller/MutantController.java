package com.mutants.challenge.controller;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mutants.challenge.model.dto.MutantRequestDto;
import com.mutants.challenge.model.entity.MutantStatistic;
import com.mutants.challenge.services.IMutantService;
import com.mutants.challenge.services.IMutantStatisticService;

@RestController
public class MutantController {
	
	@Autowired
	private IMutantService mutantService;
	
	@Autowired
	private IMutantStatisticService iMutantStatisticService; 

	
	@PostMapping("/mutant")
	public ResponseEntity<?> validMutant(@RequestBody MutantRequestDto mutantRequest){
		Map<String, Object> response = new HashMap<String, Object>();
		
		String inputErrors = mutantService.isValidDnaInput(mutantRequest.getDna()); 
		
		if(inputErrors.trim().isEmpty()) {
			
			if(mutantService.isMutant(mutantRequest)) {
				iMutantStatisticService.saveValidMutant(new MutantStatistic(mutantRequest.getDna().toString(), true));
				return ResponseEntity.status(HttpStatus.OK).body(null);
			}
			
			iMutantStatisticService.saveValidMutant(new MutantStatistic(mutantRequest.getDna().toString(), false));
			
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
		}
		
		response.put("Error-message", inputErrors);
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(inputErrors);
		
	}
	
	@GetMapping("/stats")
	public ResponseEntity<?> mutantStatistics(){
		return ResponseEntity.ok(iMutantStatisticService.findMutantStatistcis());		
	}
	
}
