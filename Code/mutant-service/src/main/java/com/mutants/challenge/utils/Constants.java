package com.mutants.challenge.utils;

public class Constants {

	public static final String PATTERN_ONLY_LETTERS = "^[a-zA-Z]+$";
	public static final String PATTERN_UPPERCASE = "^[A-Z]+$";
	public static final String PATTERN_VALID_CHARACTERS = "^[ATCG]+$";
	public static final String DNA_MUTTANT_PATTERN = "[A]{4}|[C]{4}|[G]{4}|[T]{4}";
	
	public static final int DNA_MIN_NUMBER_PATTERN = 4;
	
	
	public static final String EMPTY_MESSAGE = "Input can not be empty";
	public static final String NON_VALID_MINIM_INPUT = "Input should be more or equal to 4";
	public static final String NON_VALID_MATRIX = "The input matrix is not symetric";
	public static final String MATRIX_HAS_NUMBERS = "The input matrix has numbers";
	public static final String MATRIX_HAS_LOWER_CHAR = "The input matrix has lower chars";
	public static final String NON_VALID_CHAR_MATRIX = "The input matrix has differents chars to A-C-T-G";
	
}
