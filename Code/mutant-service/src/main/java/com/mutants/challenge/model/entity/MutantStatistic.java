package com.mutants.challenge.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mutantStatistic")
public class MutantStatistic {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="dna_request")
	private String dnaRequest;
	
	@Column(name="is_mutant")
	private boolean mutantCheckingResult;
	
	@Column(name="request_date")
	private Date requestDate;
	
	@PrePersist
	@Temporal(TemporalType.TIMESTAMP)
	public void prePersist() {
		requestDate = new Date();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDnaRequest() {
		return dnaRequest;
	}

	public void setDnaRequest(String dnaRequest) {
		this.dnaRequest = dnaRequest;
	}

	public boolean isMutantCheckingResult() {
		return mutantCheckingResult;
	}

	public void setMutantCheckingResult(boolean mutantCheckingResult) {
		this.mutantCheckingResult = mutantCheckingResult;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public MutantStatistic(String dnaRequest, boolean mutantCheckingResult) {
		super();
		this.dnaRequest = dnaRequest;
		this.mutantCheckingResult = mutantCheckingResult;
	}

	public MutantStatistic() {}

}
