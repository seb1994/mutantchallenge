package com.mutants.challenge.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mutants.challenge.model.dto.MutantStatisticDto;
import com.mutants.challenge.model.entity.MutantStatistic;
import com.mutants.challenge.repository.MutantRepository;
import com.mutants.challenge.services.IMutantStatisticService;

@Service
public class MutantStatisticServiceImpl implements IMutantStatisticService {
	
	@Autowired
	private MutantRepository mutantRepository;

	@Override
	public void saveValidMutant(MutantStatistic mutantStatistic) {
		mutantRepository.save(mutantStatistic);		
	}

	@Override
	public MutantStatisticDto findMutantStatistcis() {
		List <MutantStatistic> mutantList = (List<MutantStatistic>) mutantRepository.findAll();   
		
		if(mutantList.isEmpty()) {
			return new MutantStatisticDto(0,0,0);
		}
		
		long numMutants = mutantList.stream().filter(mu -> mu.isMutantCheckingResult()).count();
		long numNoMutants = mutantList.stream().filter(mu -> !mu.isMutantCheckingResult()).count();
		double ratio = 0;
		
		if(numMutants > 0 && numNoMutants > 0) {
			ratio = (double) numMutants / (double) numNoMutants;		
			
		}
		
		return new MutantStatisticDto(numMutants, numNoMutants, ratio);
	}

}
