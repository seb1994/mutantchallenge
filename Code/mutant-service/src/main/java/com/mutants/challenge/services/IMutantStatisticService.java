package com.mutants.challenge.services;

import com.mutants.challenge.model.dto.MutantStatisticDto;
import com.mutants.challenge.model.entity.MutantStatistic;

public interface IMutantStatisticService {

	public void saveValidMutant(MutantStatistic mutantStatistic);
	
	public MutantStatisticDto findMutantStatistcis();
}
