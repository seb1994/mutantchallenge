package com.mutants.challenge.services;

import com.mutants.challenge.model.dto.MutantRequestDto;

public interface IMutantService {
	
	public boolean isMutant(MutantRequestDto mutantRequestDto);
	
	public String isValidDnaInput(String [] dna);

}
