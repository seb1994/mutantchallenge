package com.mutants.challenge.repository;

import org.springframework.data.repository.CrudRepository;

import com.mutants.challenge.model.entity.MutantStatistic;

public interface MutantRepository extends CrudRepository<MutantStatistic, Long> {

}
