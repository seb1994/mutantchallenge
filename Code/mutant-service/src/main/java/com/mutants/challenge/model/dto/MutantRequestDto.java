package com.mutants.challenge.model.dto;

public class MutantRequestDto {

	private String [] dna;

	public String[] getDna() {
		return dna;
	}

	public void setDna(String[] dna) {
		this.dna = dna;
	}

	public MutantRequestDto(String[] dna) {
		super();
		this.dna = dna;
	}

	public MutantRequestDto() {
	
	}

}
