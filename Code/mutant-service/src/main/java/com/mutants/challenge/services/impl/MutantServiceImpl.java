package com.mutants.challenge.services.impl;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.mutants.challenge.model.dto.MutantRequestDto;
import com.mutants.challenge.services.IMutantService;
import com.mutants.challenge.utils.Constants;
import com.mutants.challenge.utils.UtilsMutant;

@Service
public class MutantServiceImpl implements IMutantService {

	@Override
	public boolean isMutant(MutantRequestDto mutantRequestDto) {
		return isMutantOrHuman(mutantRequestDto.getDna());
	}

	@Override
	public String isValidDnaInput(String[] dna) {
		
		if(dna == null || dna.length == 0) {
			return Constants.EMPTY_MESSAGE;
		}
		
		if(dna.length < Constants.DNA_MIN_NUMBER_PATTERN) {
			return Constants.NON_VALID_MINIM_INPUT;
		}
		
		for(int i = 0; i < dna.length; i++) {
			if(dna[i].length() != dna.length) {
				return Constants.NON_VALID_MATRIX;
			}
		}

		Pattern dnaPatternLetters = Pattern.compile(Constants.PATTERN_ONLY_LETTERS);
		Pattern dnaPatternUpperLetters = Pattern.compile(Constants.PATTERN_UPPERCASE);
		Pattern dnaPatternValidLetters = Pattern.compile(Constants.PATTERN_VALID_CHARACTERS);
		
		Long onlyLetters = Arrays.asList(dna)
									 .stream()
									 .filter(d -> !dnaPatternLetters.matcher(d).find())
									 .count();
		
		Long onlyUpperLetters = Arrays.asList(dna)
									  .stream()
									  .filter(d -> !dnaPatternUpperLetters.matcher(d).find())
									  .count();
		
		Long onlyDnaLetters = Arrays.asList(dna)
									.stream()
									.filter(d -> !dnaPatternValidLetters.matcher(d).find())
									.count();
		
		if(onlyLetters > 0) {
			return Constants.MATRIX_HAS_NUMBERS;
		}
		
		if(onlyUpperLetters > 0 ) {
			return Constants.MATRIX_HAS_LOWER_CHAR;
		}
		
		if(onlyDnaLetters > 0 ) {
			return Constants.NON_VALID_CHAR_MATRIX;
		}
		
		
		return "";
	}
	
	private boolean isMutantOrHuman(String[] dna) {
		long dnaNumberCoincidence = 0;
		Pattern dnaPattern = Pattern.compile(Constants.DNA_MUTTANT_PATTERN);

		//Vertical
		dnaNumberCoincidence = Arrays.asList(dna)
									 .stream()
				                     .filter(d -> dnaPattern.matcher(d).find())
				                     .count();

		if (dnaNumberCoincidence > 1) {
			return true;
		}

		//Horizontal
		String[] dnaHorizontal = UtilsMutant.transposeDna(dna);

		dnaNumberCoincidence += Arrays.asList(dnaHorizontal)
									  .stream()
									  .filter(d -> dnaPattern.matcher(d).find())
									  .count();

		if (dnaNumberCoincidence > 1) {
			return true;
		}

		//LeftObl
		String[] dnaLeftObl = UtilsMutant.oblDna(dna);

		dnaNumberCoincidence += Arrays.asList(dnaLeftObl)
									  .stream()
									  .filter(d -> dnaPattern.matcher(d).find())
									  .count();

		if (dnaNumberCoincidence > 1) {
			return true;
		}

		//RightObl
		String[] reverseDna = UtilsMutant.reverseDna(dna);

		String[] dnaRightObl = UtilsMutant.oblDna(reverseDna);

		dnaNumberCoincidence += Arrays.asList(dnaRightObl)
									  .stream()
									  .filter(d -> dnaPattern.matcher(d).find())
									  .count();

		if (dnaNumberCoincidence > 1) {
			return true;
		}

		return false;
	}

}
