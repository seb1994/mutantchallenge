package com.mutants.challenge.model.dto;

import java.io.Serializable;

public class MutantStatisticDto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private long countMutantDna;
	
	private long countHumanDna;
	
	private double ratio;

	public Long getCountMutantDna() {
		return countMutantDna;
	}

	public void setCountMutantDna(long countMutantDna) {
		this.countMutantDna = countMutantDna;
	}

	public Long getCountHumanDna() {
		return countHumanDna;
	}

	public void setCountHumanDna(long countHumanDna) {
		this.countHumanDna = countHumanDna;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
	
	public MutantStatisticDto() {
		
	};

	public MutantStatisticDto(long countMutantDna, long countHumanDna, double ratio) {
		super();
		this.countMutantDna = countMutantDna;
		this.countHumanDna = countHumanDna;
		this.ratio = ratio;
	}
	

}
